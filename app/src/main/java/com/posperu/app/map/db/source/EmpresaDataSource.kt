package com.posperu.app.map.db.source

import com.posperu.app.map.db.AppDatabase
import com.posperu.app.map.model.Empresa
import com.posperu.app.map.model.Entidad
import kotlinx.coroutines.flow.Flow

class EmpresaDataSource(db:AppDatabase):Localempresa {
    private var dao=db.empresaDao()
    override suspend fun insert(empresa: Empresa) =
        dao.insert(empresa)

    override suspend fun update(empresa: Empresa) =
        dao.update(empresa)

    override suspend fun delete(empresa: Empresa) =
        dao.delete(empresa)

    override fun listAll(): Flow<List<Empresa>> =
        dao.listAll()

    override suspend fun findById(id: Int): Empresa =
        dao.findById(id)
}

interface Localempresa {
    suspend fun insert(empresa: Empresa)
    suspend fun update(empresa: Empresa)
    suspend fun delete(empresa: Empresa)
    fun listAll():Flow<List<Empresa>>
    suspend fun findById(id:Int):Empresa
}