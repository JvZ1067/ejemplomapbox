package com.posperu.app.map.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.madapps.liquid.LiquidRefreshLayout
import com.posperu.app.map.AppMaps
import com.posperu.app.map.databinding.ActivityEmpresaBinding
import com.posperu.app.map.model.Empresa
import com.posperu.app.map.ui.adapter.EmpresaAdapter
import com.posperu.app.map.ui.adapter.OnEmpresaCallback
import com.posperu.app.map.util.collectFlow
import com.posperu.app.map.util.getViewModel
import com.posperu.app.map.viewmodel.EmpresaModelView
import kotlinx.coroutines.delay


class UI_Empresa : AppCompatActivity(),OnEmpresaCallback {

    private lateinit var binding: ActivityEmpresaBinding

    private lateinit var empresaModelView: EmpresaModelView
    var adapter: EmpresaAdapter = EmpresaAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityEmpresaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        empresaModelView=getViewModel { buildViewModel() }

        binding.recylerEmpresa.layoutManager=LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recylerEmpresa.adapter=adapter

        binding.refreshLayout.setOnRefreshListener(object : LiquidRefreshLayout.OnRefreshListener {
            override fun completeRefresh() {
                //binding.refreshLayout.finishRefreshing()
            }

            override fun refreshing() {
                loadInformation()
            }

        })
        loadInformation()


    }

    private fun loadInformation(){
        lifecycleScope.collectFlow(empresaModelView.listAll){
           adapter.submitList(it)

            delay(8000)
            binding.refreshLayout.finishRefreshing()
        }

    }

    private fun buildViewModel() = EmpresaModelView(AppMaps.database)

    override fun onSelectItem(empresa: Empresa) {
        val data = Intent()
        data.putExtra("VALUE_LONGITUDE", empresa.longitude)
        data.putExtra("VALUE_LATITUDE", empresa.latitude)
        setResult(RESULT_OK, data)
        finish()
    }

    override fun onDeleteItem(empresa: Empresa) {
        Toast.makeText(this, "Press delete item ${empresa.id}", Toast.LENGTH_LONG).show()
    }

    override fun onEditItem(empresa: Empresa) {
        Toast.makeText(this, "Press edit item ${empresa.id}", Toast.LENGTH_LONG).show()
    }
}