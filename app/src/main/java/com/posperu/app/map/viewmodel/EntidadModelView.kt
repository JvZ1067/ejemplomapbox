package com.posperu.app.map.viewmodel

import androidx.lifecycle.ViewModel
import com.posperu.app.map.db.AppDatabase
import com.posperu.app.map.db.source.EntidadDataSource
import com.posperu.app.map.model.Entidad
import kotlinx.coroutines.flow.Flow

class EntidadModelView(db:AppDatabase):ViewModel() {
    private val repo=EntidadDataSource(db)

    suspend fun insert(entidad: Entidad) =repo.insert(entidad)
    suspend fun update(entidad: Entidad) =repo.update(entidad)
    suspend fun delete(entidad: Entidad) = repo.delete(entidad)
    val listAll: Flow<List<Entidad>> get() = repo.listAll()
    suspend fun findById(id:Int): Entidad =repo.findById(id)
}