package com.posperu.app.map.viewmodel

import androidx.lifecycle.ViewModel
import com.posperu.app.map.db.AppDatabase
import com.posperu.app.map.db.source.EmpresaDataSource
import com.posperu.app.map.model.Empresa
import kotlinx.coroutines.flow.Flow

class EmpresaModelView(db:AppDatabase):ViewModel() {

    private var empresaRepo=EmpresaDataSource(db)


    suspend fun insert(empresa: Empresa) =empresaRepo.insert(empresa)
    suspend fun update(empresa: Empresa) =empresaRepo.update(empresa)
    suspend fun delete(empresa: Empresa) =empresaRepo.delete(empresa)
    val listAll: Flow<List<Empresa>>  get() =empresaRepo.listAll()
    suspend fun findById(id:Int): Empresa =empresaRepo.findById(id)


}