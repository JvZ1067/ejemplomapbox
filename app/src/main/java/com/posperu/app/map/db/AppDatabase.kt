package com.posperu.app.map.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.posperu.app.map.db.dao.EmpresaDao
import com.posperu.app.map.db.dao.EntidadDao
import com.posperu.app.map.model.Empresa
import com.posperu.app.map.model.Entidad

@Database(
    entities =
    [
        Empresa::class,
        Entidad::class
    ],
    version = 1
)
abstract class AppDatabase():RoomDatabase() {
    abstract fun empresaDao():EmpresaDao
    abstract fun entidadDao():EntidadDao
}