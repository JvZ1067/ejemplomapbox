package com.posperu.app.map.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "empresa")
data class Empresa(
    @PrimaryKey(autoGenerate = true)
    var id:Int?,

    @ColumnInfo(defaultValue = "'NULL'")
    var empresa:String,

    @ColumnInfo(defaultValue = "'NULL'")
    var ruc:String,

    @ColumnInfo(defaultValue = "'NULL'")
    var direccion:String?,

    @ColumnInfo(defaultValue = "'NULL'")
    var telefono:String?,

    @ColumnInfo(defaultValue = "'NULL'")
    var correo:String?,

    @ColumnInfo(defaultValue = "'NULL'")
    var latitude:String,

    @ColumnInfo(defaultValue = "'NULL'")
    var longitude:String,

    var estado:Boolean,

    var enviado:Boolean
)
