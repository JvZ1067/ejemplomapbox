package com.posperu.app.map.db.source

import com.posperu.app.map.db.AppDatabase
import com.posperu.app.map.model.Entidad
import kotlinx.coroutines.flow.Flow

class EntidadDataSource(db:AppDatabase) :LocalEntidad{

    private var dao=db.entidadDao()

    override suspend fun insert(entidad: Entidad) =
        dao.insert(entidad)

    override suspend fun update(entidad: Entidad) =
        dao.update(entidad)

    override suspend fun delete(entidad: Entidad) =
        dao.delete(entidad)

    override fun listAll(): Flow<List<Entidad>> =
        dao.listAll()

    override suspend fun findById(id: Int): Entidad =
        dao.findById(id)
}
interface LocalEntidad{
    suspend fun insert(entidad: Entidad)
    suspend fun update(entidad: Entidad)
    suspend fun delete(entidad: Entidad)
    fun listAll(): Flow<List<Entidad>>
    suspend fun findById(id:Int): Entidad
}