package com.posperu.app.map.ui

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.posperu.app.map.AppMaps
import com.posperu.app.map.R
import com.posperu.app.map.databinding.ActivityEmpresaRegisterBinding
import com.posperu.app.map.model.Empresa
import com.posperu.app.map.util.getViewModel
import com.posperu.app.map.viewmodel.EmpresaModelView
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class EmpresaRegister : AppCompatActivity() {

    private lateinit var binding: ActivityEmpresaRegisterBinding

    private var latitude:String ?=null
    private var longitude:String ?=null
    private var address:String ?=null

    private lateinit var empresaModelView: EmpresaModelView

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_round_arrow_back_ios_24)
        supportActionBar?.title = "Registrar Empresa"
        binding= ActivityEmpresaRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        empresaModelView=getViewModel { buildViewModel() }

        longitude= intent.getStringExtra("LONGITUDE")!!
        latitude= intent.getStringExtra("LATITUDE")!!
        address =intent.getStringExtra("ADDRESS")!!

        binding.empresaLocation.setText("$longitude , $latitude")
        binding.empresaDireccion.setText(address)

        binding.button.setOnClickListener {
            save()
        }


    }

    private fun save(){
        lifecycleScope.launch {
            val empresa=Empresa(
                    id = null,
                    ruc = binding.empresaRuc.text.toString(),
                    empresa = binding.empresaNombre.text.toString(),
                    direccion = binding.empresaDireccion.text.toString(),
                    telefono = binding.empresaTelefono.text.toString(),
                    correo = binding.empresaCorreo.text.toString(),
                    latitude = latitude!!,
                    longitude = longitude!!,
                    estado = true,
                    enviado = false
            )
            empresaModelView.insert(empresa)
            Toast.makeText(this@EmpresaRegister,"Registrado",Toast.LENGTH_LONG).show()
            delay(500)
            finish()
        }
    }

    private fun buildViewModel() = EmpresaModelView(AppMaps.database)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }
}