package com.posperu.app.map.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "entidad")
data class Entidad(

    @PrimaryKey(autoGenerate = true)
    var id:Int?,

    @ColumnInfo(defaultValue ="'NULL'")
    var dni:String?,

    @ColumnInfo(defaultValue ="'NULL'")
    var nombre:String?,

    @ColumnInfo(defaultValue ="'NULL'")
    var apellidos:String?,

    @ColumnInfo(defaultValue ="'NULL'")
    var correo:String?,

    @ColumnInfo(defaultValue ="'NULL'")
    var telefono:String?,

    @ColumnInfo(defaultValue ="'NULL'")
    var obs:String?,

    @ColumnInfo(defaultValue ="'NULL'")
    var latitude:String,

    @ColumnInfo(defaultValue ="'NULL'")
    var longitude:String,

    var activo:Boolean,
    var enviado:Boolean
)
