package com.posperu.app.map.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.posperu.app.map.R
import com.posperu.app.map.databinding.EmpresaItemsBinding
import com.posperu.app.map.model.Empresa

class EmpresaAdapter(private val onEmpresaCallback: OnEmpresaCallback):ListAdapter<Empresa,EmpresaAdapter.ViewHolder>(EmpresaDiffUtil) {



    class ViewHolder(view:View):RecyclerView.ViewHolder(view){
        private val binding=EmpresaItemsBinding.bind(view)
        fun render(empresa: Empresa,onEmpresaCallback: OnEmpresaCallback){
            binding.empresaName.text=empresa.empresa
            binding.empresaCorreo.text=empresa.correo
            binding.empresaDireccion.text=empresa.direccion
            binding.empresaTelefono.text=empresa.telefono
            binding.empresaRuc.text=empresa.ruc

            binding.btnDelete.setOnClickListener {
                onEmpresaCallback.onDeleteItem(empresa)
            }
            binding.btnEdit.setOnClickListener {
                onEmpresaCallback.onEditItem(empresa)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflate=LayoutInflater.from(parent.context).inflate(R.layout.empresa_items,parent,false)
        return ViewHolder(inflate)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val empresa=getItem(position)
        holder.render(empresa,onEmpresaCallback)
        holder.itemView.setOnClickListener {
            onEmpresaCallback.onSelectItem(empresa)
        }
    }
}
private object EmpresaDiffUtil:DiffUtil.ItemCallback<Empresa>(){
    override fun areItemsTheSame(oldItem: Empresa, newItem: Empresa): Boolean {
        return oldItem.id==newItem.id
    }

    override fun areContentsTheSame(oldItem: Empresa, newItem: Empresa): Boolean {
        return oldItem==newItem
    }

}

interface OnEmpresaCallback{
    fun onSelectItem(empresa:Empresa)
    fun onDeleteItem(empresa: Empresa)
    fun onEditItem(empresa: Empresa)
}