package com.posperu.app.map

import android.R.attr.*
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import client.yalantis.com.foldingtabbar.FoldingTabBar
import com.google.gson.JsonObject
import com.mapbox.android.core.location.*
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.api.geocoding.v5.models.CarmenFeature
import com.mapbox.geojson.*
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions
import com.mapbox.mapboxsdk.style.layers.FillLayer
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.utils.BitmapUtils
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import com.mapbox.turf.TurfConstants.UNIT_METERS
import com.mapbox.turf.TurfMeta
import com.mapbox.turf.TurfTransformation
import com.posperu.app.map.databinding.ActivityMainBinding
import com.posperu.app.map.ui.EmpresaRegister
import com.posperu.app.map.ui.UI_Empresa
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference
import java.util.*
import kotlin.coroutines.suspendCoroutine


@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity(),OnMapReadyCallback,PermissionsListener,MapboxMap.OnMapClickListener {

    private var mapView: MapView? = null
    private var mapboxMap: MapboxMap? = null

    private var permissionsManager: PermissionsManager? = null
    private var locationEngine: LocationEngine? = null
    private val callback: LocationChangeListeningActivityLocationCallback =
        LocationChangeListeningActivityLocationCallback(this)

    private val DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L
    private val DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5

    private var locationComponent: LocationComponent? = null
    private var currentRoute: DirectionsRoute? = null
    private var navigationMapRoute: NavigationMapRoute? = null

    private val circleUnit = UNIT_METERS
    private val circleSteps = 180
    private val circleRadius = 100
    private val TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID =
        "TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID"
    private val CIRCLE_CENTER_SOURCE_ID = "CIRCLE_CENTER_SOURCE_ID"
    private val CIRCLE_CENTER_ICON_ID = "CIRCLE_CENTER_ICON_ID"
    private val CIRCLE_CENTER_LAYER_ID = "CIRCLE_CENTER_LAYER_ID"
    private val TURF_CALCULATION_FILL_LAYER_ID = "TURF_CALCULATION_FILL_LAYER_ID"

    private var locationDestine: Point ?=null

    //BUSQUEDA
    private val REQUEST_CODE_AUTOCOMPLETE = 1
    private val geojsonSourceLayerId = "geojsonSourceLayerId"
    private var home: CarmenFeature? = null
    private var work: CarmenFeature? = null

    private var distance: String ?=null

    private val REQUEST_CODE_SELECT_COMPANY = 2



    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        supportActionBar?.hide()
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        mapView = binding.mapView
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync(this)

        selectNav()

        binding.btnStartRoute.setOnClickListener {
            startNav()
        }
    }

    private fun selectNav(){
        binding.foldingTabBar.onFoldingItemClickListener= object :
            FoldingTabBar.OnFoldingItemSelectedListener {
            override fun onFoldingItemSelected(item: MenuItem): Boolean {
                when(item.itemId){
                    R.id.nav_add -> {
                        if (locationDestine!=null){
                            val intent=Intent(this@MainActivity,EmpresaRegister::class.java)
                            val b=Bundle()
                            val lon=locationDestine?.longitude()
                            val lat=locationDestine?.latitude()
                            b.putString("LONGITUDE",lon.toString())
                            b.putString("LATITUDE",lat.toString())
                            b.putString("ADDRESS",getAdrress(locationDestine!!))

                            intent.putExtras(b)

                            startActivity(intent)
                        }else{
                            Toast.makeText(this@MainActivity,"Destino no seleccionado",Toast.LENGTH_LONG).show()
                        }
                    }
                    R.id.nav_company -> {
                        val i=Intent(this@MainActivity,UI_Empresa::class.java)
                        startActivityForResult(i,REQUEST_CODE_SELECT_COMPANY)
                    }
                    R.id.nav_people -> {
                        Toast.makeText(AppMaps.context, "View People", Toast.LENGTH_LONG).show()
                    }
                    R.id.nav_setting -> {
                        Toast.makeText(AppMaps.context, "Setting", Toast.LENGTH_LONG).show()
                    }
                }
                return true
            }

        }
    }
    private fun getAdrress(latLng: Point):String{
        val addresses: List<Address>
        val geocoder: Geocoder = Geocoder(this, Locale.getDefault())
        return try {
            addresses = geocoder.getFromLocation(latLng.latitude(), latLng.longitude(), 1)
            addresses[0].getAddressLine(0)
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        mapboxMap.setStyle(
            Style.Builder().fromUri(Style.MAPBOX_STREETS)
                .withImage(
                    CIRCLE_CENTER_ICON_ID, BitmapUtils.getBitmapFromDrawable(
                        resources.getDrawable(R.drawable.mapbox_marker_icon_default)
                    )!!
                )
                .withSource(GeoJsonSource(TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID))
                .withLayer(
                    SymbolLayer(
                        CIRCLE_CENTER_LAYER_ID,
                        CIRCLE_CENTER_SOURCE_ID
                    ).withProperties(
                        PropertyFactory.iconImage(CIRCLE_CENTER_ICON_ID),
                        PropertyFactory.iconIgnorePlacement(true),
                        PropertyFactory.iconAllowOverlap(true),
                        PropertyFactory.iconOffset(arrayOf(0f, -4f))
                    )
                )
        ) { style ->
            initPolygonCircleFillLayer()
            enableLocationComponent(style)
            addDestinationIconSymbolLayer(style)
            initSearchFab()
            addUserLocation()
            //
            setUpSource(style)
            setupLayer(style)
        }
        mapboxMap.addOnMapClickListener(this@MainActivity)
    }
    override fun onMapClick(point: LatLng): Boolean {
        val destinationPoint: Point = Point.fromLngLat(point.longitude, point.latitude)
        val originPoint: Point = Point.fromLngLat(
            locationComponent?.lastKnownLocation!!.longitude,
            locationComponent?.lastKnownLocation!!.latitude
        )

        val source = mapboxMap!!.style!!.getSourceAs<GeoJsonSource>("destination-source-id")

        source?.setGeoJson(Feature.fromGeometry(destinationPoint))
        getRoute(originPoint, destinationPoint)
        val lastClickPoint = Point.fromLngLat(point.longitude, point.latitude)

        locationDestine =destinationPoint

        drawPolygonCircle(lastClickPoint)
        startRoute(destinationPoint)
        return true
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            // Obtener una instancia del componente
            locationComponent = mapboxMap!!.locationComponent

            // Establecer los componentes de activación
            val locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(this, loadedMapStyle)
                    .useDefaultLocationEngine(false)
                    .build()

            locationComponent!!.activateLocationComponent(locationComponentActivationOptions)

            // hacer visible el componente
            locationComponent!!.isLocationComponentEnabled = true

            // Establecer el modo de camara del componetes
            locationComponent!!.cameraMode = CameraMode.TRACKING_GPS

            // Establecer el modo de renderizado del componente
            locationComponent!!.renderMode = RenderMode.COMPASS
            initLocationEngine()
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager!!.requestLocationPermissions(this)
        }
    }

    /**
     * Update location user
     */
    @SuppressLint("MissingPermission")
    private fun initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this)
        val request = LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
            .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
            .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build()
        locationEngine!!.requestLocationUpdates(request, callback, mainLooper)
        locationEngine!!.getLastLocation(callback)
    }
    private fun initSearchFab(){
        binding.btnSearch.setOnClickListener {
            val intent: Intent = PlaceAutocomplete.IntentBuilder()
                    .accessToken(getString(R.string.mapbox_access_token))
                    .placeOptions(
                        PlaceOptions.builder()
                            .backgroundColor(Color.parseColor("#EEEEEE"))
                            .limit(10)
                            .country("PE")
                            .addInjectedFeature(home)
                            .addInjectedFeature(work)
                            .build(PlaceOptions.MODE_CARDS)

                    )
                    .build(this@MainActivity)
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE)
        }
    }
    private fun addUserLocation(){
        home = CarmenFeature.builder().text("House")
                .geometry(
                    Point.fromLngLat(
                        locationComponent?.lastKnownLocation!!.longitude,
                        locationComponent?.lastKnownLocation!!.latitude
                    )
                )
                .placeName("My Locations")
                .id("mapbox-sf")
                .properties(JsonObject())
                .build()

        work = CarmenFeature.builder().text("Mapbox DC Office")
                .placeName("740 15th Street NW, Washington DC")
                .geometry(Point.fromLngLat(-77.0338348, 38.899750))
                .id("mapbox-dc")
                .properties(JsonObject())
                .build()
    }
    private fun setUpSource(loadedMapStyle: Style) {
        loadedMapStyle.addSource(GeoJsonSource(geojsonSourceLayerId))
    }

    private fun setupLayer(loadedMapStyle: Style) {
        loadedMapStyle.addLayer(
            SymbolLayer("SYMBOL_LAYER_ID", geojsonSourceLayerId).withProperties(
                PropertyFactory.iconImage(CIRCLE_CENTER_ICON_ID),
                PropertyFactory.iconOffset(arrayOf(0f, -8f))
            )
        )
    }

    //PERMISOS
    private fun addDestinationIconSymbolLayer(loadedMapStyle: Style) {
        loadedMapStyle.addImage(
            "destination-icon-id",
            BitmapFactory.decodeResource(
                this.resources,
                com.mapbox.mapboxsdk.R.drawable.mapbox_marker_icon_default
            )
        )
        val geoJsonSource = GeoJsonSource("destination-source-id")
        loadedMapStyle.addSource(geoJsonSource)
        val destinationSymbolLayer =
            SymbolLayer("destination-symbol-layer-id", "destination-source-id")
        destinationSymbolLayer.withProperties(
            PropertyFactory.iconImage("destination-icon-id"),
            PropertyFactory.iconAllowOverlap(true),
            PropertyFactory.iconIgnorePlacement(true)
        )

        loadedMapStyle.addLayer(destinationSymbolLayer)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionsManager?.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
        Toast.makeText(
            this, R.string.user_location_permission_explanation,
            Toast.LENGTH_LONG
        ).show()
    }
    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            mapboxMap!!.getStyle { style -> enableLocationComponent(style) }
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG)
                .show()
        }
    }

    // RUTA Y POLYLINES
    private fun getRoute(origin: Point, destination: Point) {
        NavigationRoute.builder(this)
            .accessToken(Mapbox.getAccessToken()!!)
            .origin(origin)
            .destination(destination)
            .build()
            .getRoute(object : Callback<DirectionsResponse?> {
                @SuppressLint("LogNotTimber")
                override fun onResponse(
                    call: Call<DirectionsResponse?>?,
                    response: Response<DirectionsResponse?>
                ) {
// You can get the generic HTTP info about the response
                    Log.d("TAG", "Response code: " + response.code())
                    if (response.body() == null) {
                        Log.e(
                            "TAG",
                            "No routes found, make sure you set the right user and access token."
                        )
                        return
                    } else if (response.body()!!.routes().size < 1) {
                        Log.e("TAG", "No routes found")
                        return
                    }

                    Log.e(
                        "Data",
                        "{Disctancia:${
                            response.body()!!.routes()[0].distance()
                        },Duracion:${response.body()!!.routes()[0].duration()}}"
                    )
                    distance=response.body()!!.routes()[0].distance().toString()

                    currentRoute = response.body()!!.routes()[0]

                    // Draw the route on the map

                    if (navigationMapRoute != null) {
                        navigationMapRoute!!.removeRoute()
                    } else {
                        navigationMapRoute =
                            NavigationMapRoute(
                                null,
                                mapView!!,
                                mapboxMap!!,
                                R.style.NavigationMapRoute
                            )
                    }
                    navigationMapRoute!!.addRoute(currentRoute)
                }

                @SuppressLint("LogNotTimber")
                override fun onFailure(call: Call<DirectionsResponse?>?, throwable: Throwable) {
                    Log.e("TAG", "Error: " + throwable.message)
                }
            })
    }
    private fun drawPolygonCircle(circleCenter: Point) {
        mapboxMap!!.getStyle { style ->
            val polygonArea: Polygon =
                getTurfPolygon(circleCenter, circleRadius.toDouble(), circleSteps, circleUnit)!!
            val polygonCircleSource =
                style.getSourceAs<GeoJsonSource>(TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID)
            polygonCircleSource?.setGeoJson(
                Polygon.fromOuterInner(
                    LineString.fromLngLats(TurfMeta.coordAll(polygonArea, false))
                )
            )
        }
    }
    private fun getTurfPolygon(centerPoint: Point, radius: Double, steps: Int, units: String): Polygon? {
        return TurfTransformation.circle(centerPoint, radius, steps, units)
    }
    @SuppressLint("SetTextI18n")
    private fun startRoute(p:Point){
        binding.foldingTabBar.expand()
        binding.btnStartRoute.visibility= View.VISIBLE
        binding.containerDetalle.visibility=View.VISIBLE
        //
        binding.txtDireccion.text=getAdrress(p)
        binding.txtLatitud.text=p.latitude().toString()
        binding.txtlongitud.text=p.longitude().toString()
    }
    private fun startNav(){
//        val simulateRoute = true
        val options = NavigationLauncherOptions.builder()
            .directionsRoute(currentRoute)
            .shouldSimulateRoute(false)
            .build()
        NavigationLauncher.startNavigation(this@MainActivity, options)
    }

    private fun initPolygonCircleFillLayer() {
        mapboxMap!!.getStyle { style -> // Create and style a FillLayer based on information that will come from the Turf calculation
            val fillLayer = FillLayer(
                TURF_CALCULATION_FILL_LAYER_ID,
                TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID
            )
            fillLayer.setProperties(
                PropertyFactory.fillColor(Color.parseColor("#f5425d")),
                PropertyFactory.fillOpacity(.7f)
            )
            style.addLayerBelow(fillLayer, CIRCLE_CENTER_LAYER_ID)
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {

            val selectedCarmenFeature = PlaceAutocomplete.getPlace(data)
            if (mapboxMap != null) {

                val destinationPoint: Point = selectedCarmenFeature.geometry() as Point
                renderDestinationLocation(destinationPoint)
            }
        }

        if (resultCode== RESULT_OK && requestCode==REQUEST_CODE_SELECT_COMPANY){

            val lon=data?.getStringExtra("VALUE_LONGITUDE")
            val lat=data?.getStringExtra("VALUE_LATITUDE")


            val destinationPoint: Point = Point.fromLngLat(
                    lon!!.toDouble(),
                    lat!!.toDouble()
            )
            renderDestinationLocation(destinationPoint)
        }
    }

    private fun renderDestinationLocation(location:Point){

        if (mapboxMap != null) {
            val style = mapboxMap!!.style
            if (style != null) {
                val source = style.getSourceAs<GeoJsonSource>("destination-source-id")
                source?.setGeoJson(
                        Feature.fromGeometry(location)
                )

                mapboxMap!!.animateCamera(
                        CameraUpdateFactory.newCameraPosition(
                                CameraPosition.Builder()
                                        .target(
                                                LatLng(
                                                        location.latitude(),
                                                        location.longitude()
                                                )
                                        )
                                        .zoom(14.0)
                                        .build()
                        ),
                        4000
                )

            }

            val destinationPoint: Point =location
            val originPoint: Point = Point.fromLngLat(
                    locationComponent?.lastKnownLocation!!.longitude,
                    locationComponent?.lastKnownLocation!!.latitude
            )
            getRoute(originPoint, destinationPoint)

            locationDestine=destinationPoint

            startRoute(destinationPoint)
        }
    }



    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView?.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }




    @Suppress("DEPRECATION")
    private class LocationChangeListeningActivityLocationCallback(activityWeakReference: MainActivity):LocationEngineCallback<LocationEngineResult>{

        private var weakReference = WeakReference(activityWeakReference)
        @SuppressLint("LogNotTimber")
        override fun onSuccess(result: LocationEngineResult?) {
            val activity: MainActivity = weakReference.get()!!
            val location = result!!.lastLocation ?: return
            Log.e("Location", location.toString())
            activity.mapboxMap!!.animateCamera(
                CameraUpdateFactory.newCameraPosition(
                    CameraPosition.Builder()
                        .target(
                            LatLng(
                                location.latitude,
                                location.longitude
                            )
                        )
                        .zoom(14.0)
                        .build()
                ),
                4000
            )
            if (activity.mapboxMap != null && result.lastLocation != null) {
                activity.mapboxMap!!.locationComponent.forceLocationUpdate(result.lastLocation)
            }

//            val originPoint: Point = Point.fromLngLat(
//                    location.longitude,
//                    location.latitude
//            )
//
//
//            if (activity.locationDestine!=null){
//                Log.e("XYZ origen",originPoint.toString())
//                Log.e("XYZ destine",activity.locationDestine!!.toString())
//                activity.getRoute(originPoint, activity.locationDestine!!)
//            }

           //
        }

        override fun onFailure(exception: Exception) {
            val activity: MainActivity = weakReference.get()!!
            Toast.makeText(
                activity, exception.localizedMessage,
                Toast.LENGTH_SHORT
            ).show()
        }

    }




}