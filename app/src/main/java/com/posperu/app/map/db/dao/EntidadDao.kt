package com.posperu.app.map.db.dao

import androidx.room.*
import com.posperu.app.map.model.Entidad
import kotlinx.coroutines.flow.Flow

@Dao
interface EntidadDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entidad: Entidad)

    @Update
    suspend fun update(entidad: Entidad)

    @Delete
    suspend fun delete(entidad: Entidad)

    @Query("SELECT * FROM entidad WHERE activo=1")
    fun listAll():Flow<List<Entidad>>
    
    @Query("SELECT * FROM entidad WHERE id=:id")
    suspend fun findById(id:Int):Entidad
}