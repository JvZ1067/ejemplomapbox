package com.posperu.app.map.db.dao

import androidx.room.*
import com.posperu.app.map.model.Empresa
import kotlinx.coroutines.flow.Flow

@Dao
interface EmpresaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(empresa: Empresa)

    @Update
    suspend fun update(empresa: Empresa)

    @Delete
    suspend fun delete(empresa: Empresa)

    @Query("SELECT * FROM empresa WHERE estado=1 ")
    fun listAll():Flow<List<Empresa>>

    @Query("SELECT * FROM empresa WHERE id=:id")
    suspend fun findById(id:Int):Empresa

}