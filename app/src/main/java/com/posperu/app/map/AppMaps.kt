package com.posperu.app.map

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.room.Room
import com.posperu.app.map.db.AppDatabase

class AppMaps(): Application() {

    companion object{
        lateinit var database:AppDatabase
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        database= Room.databaseBuilder(this,AppDatabase::class.java,"db_maps.db").
        build()
        context=applicationContext
    }
}